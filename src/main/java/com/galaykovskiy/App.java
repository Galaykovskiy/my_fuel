package com.galaykovskiy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App {
    private final static Logger log = LoggerFactory.getLogger(App.class);
    public static void main( String[] args ) {
        log.debug("********************************* CLASSPATH  SECTION *********************************");
        Arrays.stream(System.getProperty("java.class.path").split(";"))
                .sorted()
                .forEach(item -> log.debug("Classpath: {}", item)
                );

        log.debug("{}", "Start application");
    }


}
